#include <iostream>
#include <conio.h> //so we can call _getch()

using namespace std;

int main()
{
	// Define varables
	int input; //User input storage

	// Main Program
	cout << "Enter a number between 1 and 5: ";
	cin >> input;

	if (input >= 1 && input <= 5)
	{
		//loop a quote as many times as the user input;
		for (int i = 0; i < input; i++)
		{
			cout << "\"Those who dare to fail miserably can achieve greatly.\" \t-John F. Kennedy \n";
		}
	}
	else
	{
		cout << "Error: That wasnt a valid number. Try again later.";
	}


	_getch();
	return 0;
}